﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ServerMonitor
{
    public partial class ServerLabel : UserControl
    {
        /// <summary>
        /// The color of the indicator when the server is considered dead
        /// </summary>
        private static Color STATUS_COLOR_DEAD = Color.Red;
        /// <summary>
        /// The color of the indicator when the server is considered alive
        /// </summary>
        private static Color STATUS_COLOR_ALIVE = Color.Lime;
        /// <summary>
        /// The color of the indicator when the server has not yet been checked
        /// </summary>
        private static Color STATUS_COLOR_UNKNOWN = Color.Gray;
        /// <summary> 
        /// The color of the indicator when the server is being checked
        /// </summary>
        private static Color STATUS_COLOR_CHECKING = Color.Orange;
        /// <summary>
        /// The padding around the indicator
        /// </summary>
        private static int STATUS_INDICATOR_PADDING = 10;

        private static Font font = new Font("Arial", 13, FontStyle.Bold);

        //this is an enum to indate the status. If we haven't gotten information about the server yet we'll use UNKNOWN
        public enum ServerStatus
        {
            ALIVE,
            DEAD,
            UNKNOWN,
            CHECKING
        }

        /// <summary>
        /// The status of the server currently
        /// </summary>
        private ServerStatus status;
        /// <summary>
        /// The IP address of the server. Does not include the port
        /// </summary>
        private string ipAddress;
        /// <summary>
        /// The port the server is on
        /// </summary>
        private int port;
        private Brush statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_UNKNOWN);

        /// <summary>
        /// Gets/Sets the IP Address of this server
        /// </summary>
        public string IPAddress
        {
            get
            {
                return this.ipAddress;
            }
            set
            {
                this.ipAddress = value;
            }
        }

        /// <summary>
        /// Gets/Sets the port for this server
        /// </summary>
        public int Port
        {
            get
            {
                return this.port;
            }
            set
            {
                this.port = value;
            }
        }

        /// <summary>
        /// Gets/Sets the status of the server
        /// </summary>
        public ServerStatus Status
        {
            get
            {
                return this.status;
            }
            set
            {
                //Becuase the drawing is slow don't bother if the value hasn't changed
                if (value != this.status)
                {
                    this.status = value;

                    //We need to set the correct brush
                    switch (this.status)
                    {
                        case ServerStatus.ALIVE:
                            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_ALIVE);
                            break;
                        case ServerStatus.DEAD:
                            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_DEAD);
                            break;
                        case ServerStatus.UNKNOWN:
                            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_UNKNOWN);
                            break;
                        case ServerStatus.CHECKING:
                            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_CHECKING);
                            break;
                    }
                    //to deal with threading issues
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate { this.Refresh(); }));
                    }
                    else
                        this.Refresh();
                }
            }
        }

        /// <summary>
        /// Creates a new server label with the specified IP and port and sets the status to unknown
        /// </summary>
        /// <param name="ipAddress">The IP of the server without the port</param>
        /// <param name="port">The port of the server</param>
        public ServerLabel(string ipAddress, int port)
        {
            InitializeComponent();
            this.Status = ServerStatus.UNKNOWN;
            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_UNKNOWN);
            this.ipAddress = ipAddress;
            this.port = port;
        }

        /// <summary>
        /// Creates a new server label with the specified IP and port and sets the status to unknown
        /// </summary>
        /// <param name="ipAddress">The IP of the server without the port</param>
        /// <param name="port">The port of the server</param>
        /// <param name="status">The status of the server</param>
        public ServerLabel(string ipAddress, int port, ServerStatus status)
        {
            InitializeComponent();
            this.Status = status;
            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_UNKNOWN);
            switch (this.status)
            {
                case ServerStatus.ALIVE:
                    this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_ALIVE);
                    break;
                case ServerStatus.DEAD:
                    this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_DEAD);
                    break;
                case ServerStatus.UNKNOWN:
                    this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_UNKNOWN);
                    break;
            }

            this.ipAddress = ipAddress;
            this.port = port;
        }

        /// <summary>
        /// Creates a new server label with default settings
        /// </summary>
        public ServerLabel()
        {
            InitializeComponent();
            this.Status = ServerStatus.UNKNOWN;
            this.statusBrush = new SolidBrush(ServerLabel.STATUS_COLOR_UNKNOWN);
            this.ipAddress = "0.0.0.0";
            this.port = 0;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            int indicatorSize = Math.Min(this.Width, this.Height) - (2 * ServerLabel.STATUS_INDICATOR_PADDING); //10 pixels padding should be good yes?

            //The rectangle to fill as an indicator
            Rectangle indicatorRect = new Rectangle(ServerLabel.STATUS_INDICATOR_PADDING, ServerLabel.STATUS_INDICATOR_PADDING, indicatorSize, indicatorSize);
            //The rectangle to contain the text. This is basically beside the indicator with 10 pixels on all sides
            RectangleF textRectangle = new Rectangle(indicatorRect.Right, indicatorRect.Top, this.Width - (ServerLabel.STATUS_INDICATOR_PADDING + indicatorRect.Width), this.Height - (2 * ServerLabel.STATUS_INDICATOR_PADDING));

            //Magice to make the label draw centered
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            
            //Draw a square with the right color and position
            e.Graphics.FillRectangle(this.statusBrush, indicatorRect);
            //Draw the string damn it!
            e.Graphics.DrawString(this.ipAddress + ":" + this.port.ToString(), font, new SolidBrush(this.ForeColor), textRectangle, sf);
        }
    }
}
