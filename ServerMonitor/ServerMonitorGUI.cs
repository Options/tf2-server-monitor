﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Timers;
using System.Media;
using System.Net;
using System.IO;

namespace ServerMonitor
{
    public partial class ServerMonitorGUI : Form
    {
        private int interval;
        private int serverNumber = 0;
        private bool playsound;
        private Dictionary<string, List<ServerLabel>> dict;//new dictionary
        //static System.Timers.Timer timer;
        static System.Timers.Timer countdown;
        private int toNextUpdate;
        SoundPlayer player = new SoundPlayer("music123.wav");//will allow user choice later
        bool played = false;
        bool running = false;

        public ServerMonitorGUI()
        {
            InitializeComponent();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            string path = saveFileDialog1.FileName;
            XmlDocument cfgc = new XmlDocument();
            cfgc.LoadXml("<config>" + "<interval>90</interval>" + "<sound>true</sound>" + "<server>" + "<address>0.0.0.0</address>" + "<ports>" + "<port>27015</port>" + "</ports>"
                + "</server>" + "</config>");
            cfgc.Save(path);
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            makeRequests();
        }

        private void makeRequests()
        {
            List<string> keylist = new List<string>(dict.Keys);

            List<Task<int>> tasks = new List<Task<int>>();

            foreach (string k in keylist)
            {
                makeAsyncSteamRequest(k);
            }


            string time = "Last Update: " + DateTime.Now.ToString("T");
            if(lastUpdate.InvokeRequired)
            {
                lastUpdate.Invoke(new MethodInvoker(delegate{lastUpdate.Text = time;}));
            }
            played = false;
        }

        private async void makeAsyncSteamRequest(string k)
        {
            foreach (ServerLabel s in dict[k])
            {
                s.Status = ServerLabel.ServerStatus.CHECKING;
            }
            string url = "http://api.steampowered.com/ISteamApps/GetServersAtAddress/v0001?addr=" + k + "&format=xml";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Get;
            request.Timeout = 10000;
            request.Proxy = null;

            WebResponse response = await request.GetResponseAsync();//make the asynchronous request to the Steam API

            Stream responsestream = response.GetResponseStream();

            XmlDocument doc = new XmlDocument();
            doc.Load(responsestream);
            //parse the Xml document
            XmlNodeList ports = doc.GetElementsByTagName("gameport");
            List<int> portlist = new List<int>();
            for (int i = 0; i < ports.Count; i++)
            {
                portlist.Add(Convert.ToInt32(ports[i].InnerXml));
            }
            for (int i = 0; i < dict[k].Count; i++)//update the IP's servers
            {
                List<int> serverports = new List<int>();
                foreach (ServerLabel s in dict[k])
                {
                    serverports.Add(s.Port);
                }
                ServerLabel.ServerStatus online = ServerLabel.ServerStatus.DEAD;//if the server isn't in the list it's offline
                for (int j = 0; j < portlist.Count; j++)
                {
                    if (serverports[i] == portlist[j])
                    {
                        online = ServerLabel.ServerStatus.ALIVE;
                    }
                }
                dict[k][i].Status = online;
                if (!played)//oh no
                {
                    if (online == ServerLabel.ServerStatus.DEAD)
                    {
                        try
                        {
                            player.Play();//call for mommy
                        }
                        catch { }
                        played = true;
                    }
                }

            }
            //return 0;
        }


        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (running)
                return;
            try
            {
                //timer.Start();
                countdown.Start();
                makeRequests();
                label31.BackColor = Color.Lime;
                label31.ForeColor = SystemColors.ControlText;
                label31.Text = "Monitor Active";
                lastUpdate.Text = "Last Update: " + DateTime.Now.ToString("T");
                numberLabel.Text = "Servers: " + serverNumber;
                nextUpdate.Text = "Next Update: " + toNextUpdate;
                running = true;
            }
            catch
            {
                running = false;
                label31.BackColor = Color.Red;
                label31.ForeColor = Color.White;
                /*if (timer != null)
                {
                    timer.Stop();
                }*/
                if (countdown != null)
                {
                    countdown.Stop();
                }
            }
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!running)//if the program isn't running just clear the status of the servers
            {
                if (dict != null)
                {
                    foreach (string k in dict.Keys)
                    {
                        foreach (ServerLabel s in dict[k])
                        {
                            s.Invoke(new MethodInvoker(delegate { s.Status = ServerLabel.ServerStatus.UNKNOWN; }));
                        }
                    }
                }
                return;
            }
            //if (interval - toNextUpdate <= 2)
              //  return;
            running = false;
            try
            {
                //timer.Stop();
                countdown.Stop();
            }
            catch { }
            lastUpdate.Text = "";
            nextUpdate.Text = "";
            numberLabel.Text = "";
            toNextUpdate = interval;
            label31.BackColor = Color.Red;
            label31.ForeColor = Color.White;
            label31.Text = "Monitor Inactive";
            

            if (dict != null)
            {
                foreach (string k in dict.Keys)
                {
                    foreach (ServerLabel s in dict[k])
                    {
                        s.Invoke(new MethodInvoker(delegate {s.Status = ServerLabel.ServerStatus.UNKNOWN; }));
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //I am a potato
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Server Monitor V.1, written by The Part-Time Warrior of the BOOM! Gaming Community.", "Info");
        }

        private void openToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (running)
                return;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                XmlDocument cfg = new XmlDocument();
                cfg.Load(openFileDialog1.FileName);
                flowLayoutPanel1.Controls.Clear();

                dict = new Dictionary<string, List<ServerLabel>>();

                XmlNodeList ips = cfg.GetElementsByTagName("address");//construct lists of data from the config
                XmlNodeList intervals = cfg.GetElementsByTagName("interval");
                XmlNodeList soundoption = cfg.GetElementsByTagName("sound");
                interval = Convert.ToInt32(intervals[0].InnerXml);
                toNextUpdate = interval;

                playsound = soundoption[0].InnerXml.Equals("true", StringComparison.Ordinal);
                //Initialize the timer
                if (countdown != null)
                {
                    countdown.Close();//Close the timer if it already exists
                }
                
                /*timer = new System.Timers.Timer(interval * 1000);
                timer.AutoReset = true;
                timer.Elapsed += new ElapsedEventHandler(timerElapsed);*/

                countdown = new System.Timers.Timer(1000);
                countdown.AutoReset = true;
                countdown.Elapsed += new ElapsedEventHandler(updateCountdown);

                for (int i = 0; i < ips.Count; i++)//loop through the list of IP addresses and add their data to the dictionary
                {
                    XmlNodeList ports = ips[i].NextSibling.ChildNodes;
                    List<ServerLabel> portlist = new List<ServerLabel>();
                    for (int j = 0; j < ports.Count; j++)
                    {
                        ServerLabel foo = new ServerLabel(ips[i].InnerXml, Convert.ToInt32(ports[j].InnerXml));
                        flowLayoutPanel1.Controls.Add(foo);
                        portlist.Add(foo);
                    }
                    dict.Add(ips[i].InnerXml, portlist);
                }
                serverNumber = 0;
                foreach (string k in dict.Keys)
                {
                    serverNumber += dict[k].Count;
                }


            }
        }

        private void updateCountdown(object sender, ElapsedEventArgs e)
        { 

            toNextUpdate--;
            string time = "Next Update: " + toNextUpdate + " Seconds";
            if (nextUpdate.InvokeRequired)
            {
                nextUpdate.Invoke(new MethodInvoker(delegate { nextUpdate.Text = time; }));
                /*if (toNextUpdate <= interval - 2)
                {
                    nextUpdate.Invoke(new MethodInvoker(delegate { nextUpdate.Text = time; }));
                }
                else
                {
                    nextUpdate.Invoke(new MethodInvoker(delegate { nextUpdate.Text = "Next Update: Checking..."; }));
                }*/
            }
            if (toNextUpdate <= 1)
            {
                toNextUpdate = interval;
                makeRequests();
            }
        }

        private void createConfigToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog();
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
